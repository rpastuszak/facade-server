const chai = require('chai');
global.expect = chai.expect;

const sinon = require('sinon');
const sinonChai = require("sinon-chai");
chai.use(sinonChai);
global.sinon = sinon