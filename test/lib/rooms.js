import db from '../../lib/rooms';

describe('lib/rooms', () => {
  
  it('should have a set method', () => {
    const rooms = db();
    expect(typeof rooms.set).to.be.equal('function');
  });

  it('should have a get method', () => {
    const rooms = db();
    expect(typeof rooms.get).to.be.equal('function');
  });

  it('should set and read a value', () => {
    const rooms = db();
    rooms.set('a', 'A');
    expect(rooms.get('a')).to.be.equal('A');
  });

  it('should update and read a value', () => {
    const rooms = db();
    rooms.set('a', 'a');
    rooms.set('a', 'A');
    expect(rooms.get('a')).to.be.equal('A');
  });

  it('should return undefined for non-existing keys', () => {
    const rooms = db();
    expect(rooms.get('a')).to.be.undefined;
  });
  
  it('should expose a changes observable', () =>{
    const updateSub = sinon.stub();
    const  rooms = db();
    rooms.changes.subscribe(updateSub);
    expect(updateSub).to.not.have.been.called;
    rooms.set('a', 'A');
    expect(updateSub).to.have.been.calledOnce;
    expect(updateSub).to.have.been.calledWith({
      type: 'set',
      key: 'a',
      val: 'A'
    });
  });
});
