import by from '../../lib/by';
describe('By', () => {
  it('should return a filter function', () => {
    expect(typeof by('key')).to.be.equal('function');
  });

  it('should create a predicate for a key: value pair', ()=>{
    const expectedItem = {key: 'expectedVal'};
    const incorrectItem = {key: 'something'};

    const byKey = by('key');
    expect(byKey('expectedVal')(expectedItem)).to.be.true;
    expect(byKey('expectedVal')(incorrectItem)).to.be.false;
  });

  it('the filter function should work with [].filter', () => {
    const expectedItem = {key: 'expectedVal'};
    const input = [
      {key: 'val'},
      expectedItem
    ];
    expect(
      input.filter(by('key')('expectedVal'))
    ).to.be.deep.equal([expectedItem]);
  });
});
