import evili, {changesToSend} from '../../lib/evili';
import {Subject, TestScheduler, Scheduler, ReactiveTest} from 'rx';


const {
  onNext,
  onCompleted,
  subscribe
} = ReactiveTest;

describe('evili', ()=>{
   it('should be a function', ()=>{
      expect(evili).to.be.a('function') 
   });

   it('should connect to a database', ()=>{
     const mockPouchDB = sinon.stub();
     const dbConfig = {
       url: 'URL'
     }
     evili.__Rewire__('PouchDB', mockPouchDB);
     expect(mockPouchDB).to.not.have.been.called;
     
     evili(new Subject(), dbConfig);
     
     expect(mockPouchDB).to.have.been.calledWithNew;
     expect(mockPouchDB).to.have.been.calledOnce;
     expect(mockPouchDB).to.have.been.calledWith('URL');
     evili.__ResetDependency__('PouchDB');
   });

   it('should log an error and not subscribe if no db url is provided', ()=>{
     const mockPouchDB = sinon.stub();
     const mockLog = sinon.stub();
     const dbConfig = null;

     evili.__Rewire__('PouchDB', mockPouchDB);
     evili.__Rewire__('log', mockLog);

     expect(mockPouchDB).to.not.have.been.called;
     
     evili(new Subject(), dbConfig);
     
     expect(mockPouchDB).to.not.have.been.called;
     
     expect(mockLog).to.have.been.calledOnce;
     expect(mockLog).to.have.been.calledWith('Invalid CouchDB config: evili integration *NOT* running', dbConfig);
     
     evili.__ResetDependency__('PouchDB');
     evili.__ResetDependency__('log');
   });
   
   it('should save a document on changes[type=set]', ()=>{
     const mockPouchDB = sinon.stub();
     evili.__Rewire__('PouchDB', mockPouchDB);
     const scheduler = new TestScheduler();
     const input = scheduler.createHotObservable(
       onNext(300, {type: 'set', key: 'KEY', val: 'VAL1'}),
       onNext(400, {type: 'set', key: 'KEY', val: 'VAL2'}),
       onNext(405, {type: 'set', key: 'KEYB', val: 'VAL1B'}),
       onNext(401, {type: 'foo', key: 'KEY', val: 'VAL2'}),
       onNext(405, {type: 'set', key: 'KEYB', val: 'VAL2B'}),
       onNext(1500, {type: 'set', key: 'KEY', val: 'VAL3'}),
       onNext(4000, {type: 'set', key: 'KEYC', val: 'VAL1C'}),
       onNext(4200, {type: 'set', key: 'KEYD', val: 'VAL1D'})
     );

     const results = scheduler.startScheduler( 
      () => input.let(changesToSend(scheduler)),
      {
        created: 1,
        subscribed: 2,
        disposed: 10000
      })
     .messages
     .map(msg => msg.value.value);
     
     const expectedResults = [
       {type: 'set', key: 'KEY', val: 'VAL2'},
       {type: 'set', key: 'KEYB', val: 'VAL2B'},
       {type: 'set', key: 'KEY', val: 'VAL3'},
       {type: 'set', key: 'KEYC', val: 'VAL1C'},
       {type: 'set', key: 'KEYD', val: 'VAL1D'}
     ];
     console.log('RES:' , results)
     results.forEach( res => {
       expect(expectedResults).to.include(res);
     })
     expect(expectedResults.length).to.equal(results.length);
   })
});