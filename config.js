module.exports = {
  origins: [
    'localhost:*',
    'app.facade.photo:*',
    'http://app.facade.photo:*',
    'https://app.facade.photo/*',
    'www.facade.photo:*',
    'http://www.facade.photo:*',
    'https://www.facade.photo/*',
    'facade.photo:*',
    'http://facade.photo:*',
    'https://facade.photo/*',
    'facade.kalos.io:*',
    'http://facade.kalos.io:*',
    'https://facade.kalos.io/*',
    'facade.kalos.io.s3-website-eu-west-1.amazonaws.com:*',
    'http://facade.kalos.io.s3-website-eu-west-1.amazonaws.com:*',
    'https://facade.kalos.io.s3-website-eu-west-1.amazonaws.com/*',
    'facade-frontend.s3-website-eu-west-1.amazonaws.com:*',
    'http://facade-frontend.s3-website-eu-west-1.amazonaws.com:*',
    'https://facade-frontend.s3-website-eu-west-1.amazonaws.com/*',
    'http://facades.herokuapp.com',
    'http://localhost:3000',
    'http://192.168.*.*:3000',
    'http://www.localhost:*'
  ].join(' ')
};
