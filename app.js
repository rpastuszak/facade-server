import config from './config';
import IO from 'socket.io';
import createMessagesObservable from './lib/messagesObservable';
import db from './lib/rooms';
import handleRoom from './lib/handleRoom';
import by from './lib/by';
const byID = by('id');
import http from 'http';

const getEviliDBConfig = () => {
  return {
    url: process.env.FACADES_EVILI_DB_URL
  }
};

import evili from './lib/evili';

module.exports = function app() {
  const server = http.createServer(function noop() {});
  const port = process.env.PORT || 3001;
  server.listen(port);

  const io = IO(server, config);
  const rooms = db();
  evili(rooms.changes, getEviliDBConfig());

  io.on('connection', socket => {
    const messages = createMessagesObservable(socket).share();
    const handleCurrentRoom = handleRoom.bind(null, socket, messages, rooms);
    const byRoomJoined = byID('room:join');
    const getRoom = message => message.payload.room;

    messages.filter( byRoomJoined )
            .map( getRoom )
            .subscribe( handleCurrentRoom );
  });

  console.log('App initialised using port: ' + port);
}