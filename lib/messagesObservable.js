import {Observable} from 'rx';
export default function createMessagesObservable(socket) {
  return Observable.create((observer)=>{
    socket.on('message', (id, payload)=>{
      observer.onNext({socket, id, payload});
    });
  });
}
