import by from './by';
const byID = by('id');

function broadcastToRoom(room, {socket, id, payload}) {
  socket.broadcast.to(room).emit(id, payload);
}

const id2room = id => id.replace('/#', '');

function getRoom(requestedRoom, socket) {
  const {id} = socket;
  const defaultRoom = id2room(id);
  return requestedRoom === 'public' ? defaultRoom : requestedRoom;
}


export default function handleRoom(socket, messages, rooms, requestedRoom) {
  console.log(`Client requested room ${requestedRoom}`);
  const room = getRoom(requestedRoom, socket);

  const broadcast = broadcastToRoom.bind(null, room);
  console.log(`Client joined room ${room}`);

  socket.join(room);

  broadcast({id: 'client:joined', socket, payload: {id: room}});
  socket.emit('room:joined', {room});

  if (rooms.get(room)) {
    const payload = rooms.get(room);
    socket.emit('colour:update_all', payload);
  }

  const colourUpdateMessages = messages
      .filter( byID('colour:update_all') )
      .share();

  colourUpdateMessages.subscribe( updateMessage => {
    broadcast(updateMessage);
    rooms.set(room, updateMessage.payload);
  });
}
