export default function saveOrUpdateDoc(db, id, val) {
  return db.get(id).then( doc => {
      console.log('adding...')
      return db.put(val, id, doc._rev);
    } )
    .catch( err => {
      if (err && err.reason === 'missing' ) {
        console.log('ADDING NEW ', val);
        return db.put(val, id);
      } else {
        console.log(err);
      }
    });
}