import {Subject} from 'rx';

export default function init() {
  
  const changes = new Subject();
  
  const api = new Map();
  const get = key => api.get(key);
  const set = (key, val) => {
    api.set(key, val);
    changes.onNext({
       type: 'set',
       key,
       val
    });
  }

    

  return {
    api,
    get,
    set,
    changes
  }
};
