const by = key => val => obj => obj[key] === val;
export default by;
