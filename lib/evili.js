import PouchDB from 'pouchdb';
import {Observable, Scheduler} from 'rx'
const log = console.log.bind(console);

function isValidDBConfig(dbConfig) {
  return !!(dbConfig && dbConfig.url);
}

import saveOrUpdateDoc from './evili/save-or-update-doc';
const errorMessage = {
  INVALID_DB_URL: 'Invalid CouchDB config: evili integration *NOT* running'
}

// const hasKey = (arr, key) => 
function getLastUniqueChanges(updates) {
  const objectsToSave = updates
    .reduceRight((selected, curr) => {
      const {key} = curr;
      if (selected[key]) return selected;
      selected[key] = curr;
      return selected;
    }, {});
  
  return Object.keys(objectsToSave)
                .map( k => objectsToSave[k]);
}

export const changesToSend = scheduler => changes => {  
  return changes
    .filter( change => change.type && change.type === 'set')
    .bufferWithTime(1000, scheduler)
    .filter( vals => !!vals.length)
    .map( getLastUniqueChanges )    
    .flatMapLatest( results => Observable.create(obs => {
      results.forEach(obs.onNext.bind(obs))
    }));
    
}


export default function evili(
    changes,
    dbConfig,
    scheduler
  ) {
  scheduler = scheduler || Scheduler.default;
  if (!isValidDBConfig(dbConfig)) {
    log(errorMessage.INVALID_DB_URL, dbConfig);
    return;
  }
 
  const db = new PouchDB(dbConfig.url);

  return changes
    .let(changesToSend(scheduler))
    .subscribe( change => {
      saveOrUpdateDoc(
        db,
        change.key,
        change.val
      );
    });
  
}